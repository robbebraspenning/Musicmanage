﻿using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MusicManager.BL.Domain;

namespace MusicManager.DAL.EF
{
    public class MusicManagerDbContext : DbContext
    {
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Song> Songs { get; set; }
        public DbSet<RecordLabel> RecordLabels { get; set; }
        public DbSet<SongContribution> SongContributions { get; set; }
        
        public DbSet<Manager> Managers { get; set; }


        public MusicManagerDbContext(DbContextOptions<MusicManagerDbContext> options) : base(options)
        {
            MusicManagerInitializer.Initialize(this,false);
        }
        
        public MusicManagerDbContext()
        {
            MusicManagerInitializer.Initialize(this,true);
        }

        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite(@"Data Source=..\MusicManager.db");

                optionsBuilder.LogTo(msg => Debug.WriteLine(msg), LogLevel.Information);
            }
        }
        */
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            // Explicit relations
            modelBuilder.Entity<SongContribution>()
                .HasOne(sc => sc.Song )
                .WithMany(s => s.SongContributions)
                .HasForeignKey("FK_SongId")
                .IsRequired();
            
            modelBuilder.Entity<SongContribution>()
                .HasOne(sc => sc.Artist )
                .WithMany(a => a.SongContributions)
                .HasForeignKey("FK_ArtistId")
                .IsRequired();

            modelBuilder.Entity<Artist>().HasOne(a=>a.SignedRecordLabel).WithMany(rl => rl.Artists);

            modelBuilder.Entity<Artist>().HasOne(a => a.Manager).WithOne(m => m.Artist).HasForeignKey<Manager>("FK_ArtistId");
            
            // PKs
            modelBuilder.Entity<Artist>().HasKey(a => a.Id);
            modelBuilder.Entity<Song>().HasKey(s => s.Id);
            modelBuilder.Entity<RecordLabel>().HasKey(rl => rl.Id);
            modelBuilder.Entity<SongContribution>().HasKey(new string []{"FK_SongId","FK_ArtistId"});


        }
    }
}