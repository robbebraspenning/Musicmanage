﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MusicManager.BL.Domain;

namespace MusicManager.DAL.EF
{
    public class Repository : IRepository
    {
        private MusicManagerDbContext _dbContext;

        public Repository(MusicManagerDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        // Artists
        
        public Artist ReadArtist(int id)
        {
            return _dbContext.Artists.Find(id);
        }
        
        public Artist ReadArtistWithRecordLabelAndManager(int id)
        {
            IQueryable<Artist> artists = _dbContext.Artists.Include(a => a.SignedRecordLabel).Include(a => a.Manager);
            return artists.First(a => a.Id == id);
        }

        public IEnumerable<Artist> ReadAllArtists()
        {
            return _dbContext.Artists.AsEnumerable();
        }

        public IEnumerable<Artist> ReadAllArtistsWithRecordLabel()
        {
            return _dbContext.Artists.Include(a => a.SignedRecordLabel).Include(a => a.Manager).AsEnumerable();
        }
        
        public IEnumerable<Artist> ReadArtistsByAliasOrByAmountOfFollowers(string alias = null, int? amountOfFollowers = null)
        {
            IQueryable<Artist> artists = _dbContext.Artists;
            if (!String.IsNullOrEmpty(alias))
            {
                artists = _dbContext.Artists.Where(a => a.Alias.ToLower().Contains(alias.ToLower()));
            }
            if (amountOfFollowers.HasValue && amountOfFollowers > 0)
            {
                artists = artists.Where(a => a.AmountOfFollowers >= amountOfFollowers);
            }
            
            
            return artists.Include(a=>a.SignedRecordLabel).Include(a => a.Manager);
        }

        public void CreateArtist(Artist artist)
        {
            _dbContext.Artists.Add(artist);
            _dbContext.SaveChanges();
        }

        // Song
        
        public Song ReadSong(int id)
        {
            return _dbContext.Songs.Find(id);
        }

        public Song ReadSongWithArtists(int id)
        {
            IQueryable<Song> songs = _dbContext.Songs.Include(s => s.SongContributions).ThenInclude(sc => sc.Artist);
            return songs.First(s => s.Id == id);
        }

        public IEnumerable<Song> ReadAllSongs()
        {
            return _dbContext.Songs.AsEnumerable();
        }

        public IEnumerable<Song> ReadAllSongsWithArtists()
        {
            return _dbContext.Songs.Include(s => s.SongContributions).ThenInclude(sc => sc.Artist);
        }

        public IEnumerable<Song> ReadSongsOfArtist(int artistId)
        {
            IQueryable<Song> songs = _dbContext.Songs.Include(s => s.SongContributions).ThenInclude(sc => sc.Artist);
            return songs.Where(s => s.SongContributions.Any(sc => sc.Artist.Id == artistId));
        }

        public IEnumerable<Song> ReadSongByGenre(Genre genre)
        {
            IQueryable<Song> songs = _dbContext.Songs;
            return songs.Where(song => song.Genre == genre).Include(s=>s.SongContributions).ThenInclude(sc=>sc.Artist);
        }

        public IEnumerable<Song> ReadSongsNotOfArtist(int artistId)
        {
            IQueryable<Song> songs = _dbContext.Songs.Include(s => s.SongContributions).ThenInclude(sc => sc.Artist);
            return songs.Where(s => !s.SongContributions.Any(sc => sc.Artist.Id == artistId));
        }

        public void CreateSong(Song song)
        {
            _dbContext.Songs.Add(song);
            _dbContext.SaveChanges();
        }

        // RecordLabel
        
        public RecordLabel ReadRecordLabel(int id)
        { 
            return _dbContext.RecordLabels.Find(id);
        }

        public IEnumerable<RecordLabel> ReadAllRecordLabels()
        {
            return _dbContext.RecordLabels.AsEnumerable();
        }

        public void CreateRecordLabel(RecordLabel recordLabel)
        {
            _dbContext.RecordLabels.Add(recordLabel);
            _dbContext.SaveChanges();
        }

        public void UpdateRecordLabel(int id, RecordLabel updatedRecordLabel)
        {
            RecordLabel recordLabelToUpdate = _dbContext.RecordLabels.Find(id);
            recordLabelToUpdate.Name = updatedRecordLabel.Name;
            recordLabelToUpdate.DateFounded = updatedRecordLabel.DateFounded;
            recordLabelToUpdate.Location = updatedRecordLabel.Location;

            _dbContext.SaveChanges();
        }

        // Manager
        
        public Manager ReadManager(int id)
        {
            return _dbContext.Managers.Find(id);
        }

        public IEnumerable<Manager> ReadAllManagers()
        {
            return _dbContext.Managers;
        }

        public void CreateManager(Manager manager)
        {
            _dbContext.Managers.Add(manager);
            _dbContext.SaveChanges();
        }

        // SongContribution

        public SongContribution ReadSongContribution(int songId, int artistId)
        {
            return _dbContext.SongContributions.Find(songId,artistId);
        }
        
        public void CreateSongContribution(SongContribution songContribution)
        {
            _dbContext.SongContributions.Add(songContribution);
            _dbContext.SaveChanges();
        }

        public void DeleteSongContribution(int songId, int artistId)
        {
            _dbContext.SongContributions.Remove(ReadSongContribution(songId,artistId));
            _dbContext.SaveChanges();
        }
    }
}