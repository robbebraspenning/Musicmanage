﻿using System;
using System.Collections.Generic;
using System.Linq;
using MusicManager.BL.Domain;

namespace MusicManager.DAL
{
    public class InMemoryRepository : IRepository
    {
        
        // IList is used to take advantage of the indexing in the Seed-method. As well as to use the .Count()-function.
        private IList<Artist> _artists;
        private IList<Song> _songs;
        private IList<RecordLabel> _recordLabels;
        private IList<Manager> _managers;

        public InMemoryRepository()
        {
            Seed();
        }

        void Seed()
        {
            _artists = new List<Artist>();
            _recordLabels = new List<RecordLabel>();
            _songs = new List<Song>();
            _managers = new List<Manager>();


            // Record Labels
            RecordLabel recordLabel1 = new RecordLabel("YEAR0001",
                new DateTime(2015, 1, 1), "Stockholm - Sweden", null);
            RecordLabel recordLabel2 = new RecordLabel("Universal",
                new DateTime(1981, 12, 5), "New York - USA", null);
            RecordLabel recordLabel3 = new RecordLabel("Pirate Gang",
                new DateTime(2021, 10, 1), "Antwerp - Belgium", null);

            CreateRecordLabel(recordLabel1);
            CreateRecordLabel(recordLabel2);
            CreateRecordLabel(recordLabel3);
            
            
            // Managers
            Manager manager1 = new Manager("Dirk", "Janssens");
            Manager manager2 = new Manager("Thibault", "Poels");
            Manager manager3 = new Manager("Jim", "Halpert");
            Manager manager4 = new Manager("Michael", "Scott");
            Manager manager5 = new Manager("Rick", "Owens");
            
            CreateManager(manager1);
            CreateManager(manager2);
            CreateManager(manager3);
            CreateManager(manager4);
            CreateManager(manager5);
            

            // Artists

            Artist artist1 = new Artist("Benjamin Reichwald", "Bladee", null, 0, 215353, recordLabel1,manager1);
            Artist artist2 = new Artist("Barry White", "Barry White", null, 0, 1364554, recordLabel2,manager2);
            Artist artist3 = new Artist("Randy Goffe", "Home", null, 0, 212127, recordLabel2,manager3);
            Artist artist4 = new Artist("Jonatan Leandoer", "Yung Lean", null, 0, 590881, recordLabel1,manager4);
            Artist artist5 = new Artist("Jakub", "Sheg", null, 0, 93, recordLabel3,manager5);
            
            CreateArtist(artist1);
            CreateArtist(artist2);
            CreateArtist(artist3);
            CreateArtist(artist4);
            CreateArtist(artist5);

            // Songs

            Song song1 = new Song("Hennesy & Sailer Moon", 14034610,
                new DateTime(2016, 4, 19), new TimeSpan(0, 4, 14), null, Genre.Rnb);
            Song song2 = new Song("Let's Ride", 14034610,
                new DateTime(2021, 5, 30), new TimeSpan(0, 2, 25), null, Genre.Rap);
            Song song3 = new Song("Resonance", 93486077,
                new DateTime(2014, 2, 10), new TimeSpan(0, 3, 32), null, Genre.Edm);
            Song song4 = new Song("You're the first, you're the last, my everything", 168196198,
                new DateTime(1974, 2, 1), new TimeSpan(0, 3, 32), null, Genre.Other);
            Song song5 = new Song("Fairy Tale", 482,
                new DateTime(2021, 4, 1), new TimeSpan(0, 3, 10), null, Genre.Rap);

            CreateSong(song1);
            CreateSong(song2);
            CreateSong(song3);
            CreateSong(song4);
            CreateSong(song5);

            // Connections
            // Record Labels - Artists
            recordLabel1.Artists.Add(artist1);
            recordLabel1.Artists.Add(artist4);
            recordLabel2.Artists.Add(artist2);
            recordLabel2.Artists.Add(artist3);
            recordLabel3.Artists.Add(artist5);
            
            // Managers - Artists
            manager1.Artist = artist1;
            manager2.Artist = artist2;
            manager3.Artist = artist3;
            manager4.Artist = artist4;
            manager5.Artist = artist5;

            // Artists - (SongContributions) - Songs
            SongContribution songContribution1 = new SongContribution()
            {
                Artist = artist1,
                Contribution = SongContributionType.Vocalist,
                Song = song1
            };
            SongContribution songContribution2 = new SongContribution()
            {
                Artist = artist4,
                Contribution = SongContributionType.Vocalist,
                Song = song1
            };
            SongContribution songContribution3 = new SongContribution()
            {
                Artist = artist1,
                Contribution = SongContributionType.Vocalist,
                Song = song2
            };
            SongContribution songContribution4 = new SongContribution()
            {
                Artist = artist3,
                Contribution = SongContributionType.AudioEngineer,
                Song = song3
            };
            SongContribution songContribution5 = new SongContribution()
            {
                Artist = artist2,
                Contribution = SongContributionType.Vocalist,
                Song = song4
            };
            SongContribution songContribution6 = new SongContribution()
            {
                Artist = artist5,
                Contribution = SongContributionType.BeatMaker,
                Song = song5
            };



            song1.SongContributions.Add(songContribution1);
            song1.SongContributions.Add(songContribution2);
            song2.SongContributions.Add(songContribution3);
            song3.SongContributions.Add(songContribution4);
            song4.SongContributions.Add(songContribution5);
            song5.SongContributions.Add(songContribution6);
            
            // Songs - (SongContributions) - Artists
            artist1.SongContributions.Add(songContribution1);
            artist1.SongContributions.Add(songContribution3);
            artist2.SongContributions.Add(songContribution5);
            artist3.SongContributions.Add(songContribution4);
            artist4.SongContributions.Add(songContribution2);
            artist5.SongContributions.Add(songContribution6);

        }
        
        // Artist
        
        public Artist ReadArtist(int id)
        {
            foreach(Artist artist in _artists)
            {
                if (artist.Id == id) return artist;
            }

            return null;
        }

        public Artist ReadArtistWithRecordLabelAndManager(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Artist> ReadAllArtists()
        {
            return _artists;
        }

        public IEnumerable<Artist> ReadAllArtistsWithRecordLabel()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Artist> ReadArtistsByAliasOrByAmountOfFollowers(string alias = null, int? amountOfFollowers = null)
        {
            IEnumerable<Artist> artists = _artists;
            
            if (!String.IsNullOrEmpty(alias))
            {
                artists = _artists.Where(a => a.Alias.ToLower().Contains(alias.ToLower()));
            }
            if (amountOfFollowers.HasValue && amountOfFollowers > 0)
            {
                artists = artists.Where(a => a.AmountOfFollowers >= amountOfFollowers);
            }
            
            return artists;
        }

        public void CreateArtist(Artist artist)
        {
            artist.Id = _artists.Count + 1;
            _artists.Add(artist);
        }

        // Song
        
        public Song ReadSong(int id)
        {
            foreach(Song song in _songs)
            {
                if (song.Id == id) return song;
            }

            return null;
        }

        public Song ReadSongWithArtists(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Song> ReadAllSongs()
        {
            return _songs;
        }

        public IEnumerable<Song> ReadAllSongsWithArtists()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Song> ReadSongsOfArtist(int artistId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Song> ReadSongByGenre(Genre genre)
        {
            IList<Song> resultList = new List<Song>();
            
            foreach (Song song in _songs)
            {
                if (song.Genre == genre)
                {
                    resultList.Add(song);
                }
            }

            return resultList;
        }

        public IEnumerable<Song> ReadSongsNotOfArtist(int artistId)
        {
            throw new NotImplementedException();
        }

        public void CreateSong(Song song)
        {
            song.Id = _songs.Count + 1;
            _songs.Add(song);
        }
        
        // RecordLabel

        public RecordLabel ReadRecordLabel(int id)
        {
            foreach(RecordLabel recordLabel in _recordLabels)
            {
                if (recordLabel.Id == id) return recordLabel;
            }

            return null;
        }

        public IEnumerable<RecordLabel> ReadAllRecordLabels()
        {
            return _recordLabels;
        }

        public void CreateRecordLabel(RecordLabel recordLabel)
        {
            recordLabel.Id = _recordLabels.Count + 1;
            _recordLabels.Add(recordLabel);
        }

        public void UpdateRecordLabel(int id, RecordLabel updatedRecordLabel)
        {
            throw new NotImplementedException();
        }

        // Manager
        
        public Manager ReadManager(int id)
        {
            foreach(Manager manager in _managers)
            {
                if (manager.Id == id) return manager;
            }

            return null;
        }

        public IEnumerable<Manager> ReadAllManagers()
        {
            return _managers;
        }

        public void CreateManager(Manager manager)
        {
            manager.Id = _managers.Count + 1;
            _managers.Add(manager);
        }

        // SongContribution
        
        public SongContribution ReadSongContribution(int songId, int artistId)
        {
            throw new NotImplementedException();
        }

        public void CreateSongContribution(SongContribution songContribution)
        {
            throw new NotImplementedException();
        }

        public void DeleteSongContribution(int songId, int artistId)
        {
            throw new NotImplementedException();
        }
    }
}