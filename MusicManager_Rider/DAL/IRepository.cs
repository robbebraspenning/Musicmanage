﻿using System.Collections.Generic;
using MusicManager.BL.Domain;

namespace MusicManager.DAL
{
    public interface IRepository
    {
        // Artist
        
        public Artist ReadArtist(int id);

        public Artist ReadArtistWithRecordLabelAndManager(int id);
        
        public IEnumerable<Artist> ReadAllArtists();

        public IEnumerable<Artist> ReadAllArtistsWithRecordLabel();

        public IEnumerable<Artist> ReadArtistsByAliasOrByAmountOfFollowers(string alias = null, int? amountOfFollowers = null);

        public void CreateArtist(Artist artist);
        
        // Song
        
        public Song ReadSong(int id);

        public Song ReadSongWithArtists(int id);
        
        public IEnumerable<Song> ReadAllSongs();
        
        public IEnumerable<Song> ReadAllSongsWithArtists();

        public IEnumerable<Song> ReadSongsOfArtist(int artistId);

        public IEnumerable<Song> ReadSongByGenre(Genre genre);

        public IEnumerable<Song> ReadSongsNotOfArtist(int artistId);

        public void CreateSong(Song song);
        
        // RecordLabel
        
        public RecordLabel ReadRecordLabel(int id);
        
        public IEnumerable<RecordLabel> ReadAllRecordLabels();

        public void CreateRecordLabel(RecordLabel recordLabel);

        public void UpdateRecordLabel(int id, RecordLabel updatedRecordLabel);
        
        // Manager
        
        public Manager ReadManager(int id);
        
        public IEnumerable<Manager> ReadAllManagers();

        public void CreateManager(Manager manager);
        
        // SongContribution

        public SongContribution ReadSongContribution(int songId, int artistId);
        public void CreateSongContribution(SongContribution songContribution);
        public void DeleteSongContribution(int songId, int artistId);
    }
}