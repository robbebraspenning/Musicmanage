﻿namespace MusicManager.BL.Domain
{
    public enum Genre : byte
    {
        Rap = 1,
        Hyperpop,
        Pop,
        Rnb,
        Edm,
        Rock,
        Jazz,
        Classical,
        Other
    }
}