﻿using System.ComponentModel.DataAnnotations;

namespace MusicManager.BL.Domain
{
    public class SongContribution
    {
        [Required] 
        public Song Song { get; set; }
        
        [Required]
        public Artist Artist { get; set; }

        public SongContributionType Contribution { get; set; }
    }
}