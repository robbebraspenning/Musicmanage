﻿namespace MusicManager.BL.Domain
{
    public enum SongContributionType : byte
    {
        Vocalist = 1,
        AudioEngineer,
        BeatMaker,
        Mixer,
        Other
    }
}