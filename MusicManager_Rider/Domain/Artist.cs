﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MusicManager.BL.Domain
{
    
    public class Artist
    {

        public int Id { get; set; }
        public string Name { get; set; }
        
        [Required]
        [MinLength(1,ErrorMessage = "The alias of an artist must be at least 1 character long."),
         MaxLength(50,ErrorMessage = "The alias of an artist has a maximum length of 50 characters.")]
        public string Alias { get; set; }
        public IList<SongContribution> SongContributions { get; set; }
        public float TotalRevenue { get; set; }
        
        [Range(0,Int32.MaxValue, ErrorMessage = "The amount of followers can not be negative!")]
        public int AmountOfFollowers { get; set; }
        public RecordLabel SignedRecordLabel { get; set; }
        
        public Manager Manager { get; set; }

        public Artist() { }

        public Artist(string name, string alias, IList<SongContribution> songContributions, float totalRevenue, int amountOfFollowers, RecordLabel signedRecordLabel, Manager manager)
        {
            Name = name;
            Alias = alias;
            SongContributions = songContributions ?? new List<SongContribution>();
            TotalRevenue = totalRevenue;
            AmountOfFollowers = amountOfFollowers;
            SignedRecordLabel = signedRecordLabel;
            Manager = manager;
        }
    }
}