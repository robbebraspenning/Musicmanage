﻿using Microsoft.AspNetCore.Mvc;
using MusicManager.BL;
using MusicManager.BL.Domain;

namespace UI.MVC.Controllers
{
    public class SongController : Controller
    {
        private readonly IManager _manager;

        public SongController(IManager manager)
        {
            _manager = manager;
        }
        
        // GET
        public IActionResult Index()
        {
            return View(_manager.GetAllSongs());
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult Add(Song song)
        {
            if (!ModelState.IsValid)
            {
                return View(song);
            }
            
            Song createdSong = _manager.AddSong(song.Title, song.AmountOfStreams, song.ReleaseDate, song.Length, null, song.Genre);
            
            return RedirectToAction("Detail","Song",new {id = createdSong.Id});
        }

        public IActionResult Detail(int id)
        {
            return View(_manager.GetSongWithArtists(id));
        }
    }
}