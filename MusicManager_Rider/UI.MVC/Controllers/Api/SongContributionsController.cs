﻿using System;
using Microsoft.AspNetCore.Mvc;
using MusicManager.BL;
using MusicManager.BL.Domain;
using UI.MVC.Models;

namespace UI.MVC.Controllers.Api
{
    [Route("api/[controller]")]
    public class SongContributionsController : ControllerBase
    {
        private readonly IManager _manager;

        public SongContributionsController(IManager manager)
        {
            _manager = manager;
        }
        
        // GET
        [HttpGet("{songId}/{artistId}")]
        public IActionResult Get(int songId, int artistId)
        {
            if (_manager.GetSongContribution(songId,artistId) != null)
            {
                return NoContent();
            }
            
            return Ok(_manager.GetSongContribution(songId,artistId));
        }
        
        [HttpGet("songContributionTypes")]
        public IActionResult GetSongContributionTypes()
        {
            return Ok(Enum.GetNames<SongContributionType>());
        }
        
        
        [HttpPost]
        public IActionResult Post([FromBody] SongContributionDto songContributionDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Enum.TryParse(songContributionDto.ContributionType, out SongContributionType songContributionType);
            
            SongContribution songContribution = _manager.AddSongContribution(songContributionDto.SongId,songContributionDto.ArtistId,songContributionType);

            return CreatedAtAction("Get",
                new {songId = songContribution.Song.Id, artistId = songContribution.Artist.Id});
        }
    }
}