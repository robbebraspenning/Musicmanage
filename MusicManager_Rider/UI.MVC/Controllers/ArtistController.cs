﻿using Microsoft.AspNetCore.Mvc;
using MusicManager.BL;

namespace UI.MVC.Controllers
{
    public class ArtistController : Controller
    {
        private readonly IManager _manager;

        public ArtistController(IManager manager)
        {
            _manager = manager;
        }
        
        // GET
        public IActionResult Detail(int id)
        {
            return View(_manager.GetArtistWithRecordLabelAndManager(id));
        }
    }
}