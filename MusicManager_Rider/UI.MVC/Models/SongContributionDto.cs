namespace UI.MVC.Models
{
    public class SongContributionDto
    {
        public int SongId { get; set; }
        
        public int ArtistId { get; set; }

        public string ContributionType { get; set; }
    }
}