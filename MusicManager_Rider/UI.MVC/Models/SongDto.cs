﻿using System;
using System.ComponentModel.DataAnnotations;
using MusicManager.BL.Domain;

namespace UI.MVC.Models
{
    public class SongDto
    {
        public int Id { get; set; }
        
        [Required]
        [MinLength(1,ErrorMessage = "The title of a song must be at least 1 character long."),
         MaxLength(100,ErrorMessage = "The title of a song has a maximum length of 100 characters.")]
        public string Title { get; set; }
        
        [Range(0,Int32.MaxValue,ErrorMessage = "The amount of streams can not be negative!")]
        public long? AmountOfStreams { get; set; } // If the song is not yet released, this could be Null.
        
        public DateTime ReleaseDate { get; set; }
        
        public TimeSpan Length { get; set; }

        [Required]
        [Range(1,9,ErrorMessage = "The genre given was not valid!")]
        public Genre Genre { get; set; }
    }
}