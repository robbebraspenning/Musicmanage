﻿const urlParams = new URLSearchParams(window.location.search);
const artistId = parseInt(urlParams.get("artistId"));
const addSongButton = document.getElementById("addSong");

// Load songs of artist
window.addEventListener('load',loadSongsOfArtist);

// Load list of addable songs
window.addEventListener('load',loadAddSongList);

// Load list of addable songs
window.addEventListener('load',loadSongContributionTypes);

addSongButton.addEventListener('click',addSongToArtist)



function loadSongsOfArtist(){
    fetch(`/api/Songs/ofArtist/${artistId}`,
        {
            method: "GET",
            headers: {
                "Accept":"application/json"
            }
        })

        .then(response => response.json())
        .then(data => showSongsOfArtist(data))
        .catch(reason => alert("Call failed: " + reason));
}

function showSongsOfArtist(songs) {
    const tableBody = document.getElementById("songsOfArtistTableBody");
    tableBody.innerHTML = "";

    songs.forEach(song => addSong(song));
}

function addSong(song) {
    const tableBody = document.getElementById("songsOfArtistTableBody");
    const amountOfStreams = song.amountOfStreams != null ? song.amountOfStreams : "None";
    tableBody.innerHTML += `<tr> <td>${song.title}</td> <td>${formatTime(song.length)}</td> 
    <td>${amountOfStreams}</td> </td><td><a href="/Song/Detail/${song.id}">Details</a></td> </tr>`;
}

function formatTime(timespan){
    const time_minutes = `${timespan.minutes}`;
    const time_seconds = `${timespan.seconds}`;
    let time_result;
    
    if(time_minutes.length === 1){
        time_result = "0".concat(time_minutes);
    } else {
        time_result = time_minutes;
    }

    if(time_seconds.length === 1){
        time_result = time_result.concat(":0",time_seconds);
    } else {
        time_result = time_result.concat(":",time_seconds);
    }
    
    return time_result;
}

function loadAddSongList(){
    fetch(`/api/Songs/notOfArtist/${artistId}`,
        {
            method: "GET",
            headers: {
                "Accept":"application/json"
            }
        })

        .then(response => response.json())
        .then(data => showSongsNotOfArtistInSelect(data))
        .catch(reason => alert("Call failed: " + reason));
}

function showSongsNotOfArtistInSelect(songs){
    const songSelection = document.getElementById("selectSong");
    songSelection.innerHTML = "";

    songs.forEach(song => showSongNotOfArtistInSelect(song));
}

function showSongNotOfArtistInSelect(song){
    const songSelection = document.getElementById("selectSong");
    songSelection.innerHTML += `<option value="${song.id}">${song.title}</option>`;
}

function loadSongContributionTypes(){
    fetch(`/api/SongContributions/songContributionTypes`,
        {
            method: "GET",
            headers: {
                "Accept":"application/json"
            }
        })

        .then(response => response.json())
        .then(data => showSongContributionTypes(data))
        .catch(reason => alert("Call failed: " + reason));
}

function showSongContributionTypes(songContributionTypes){
    const songSelection = document.getElementById("selectContributionType");
    songSelection.innerHTML = "";

    songContributionTypes.forEach(songContribution => showSongContributionInSelect(songContribution));
}

function showSongContributionInSelect(songContribution){
    const songContributionSelection = document.getElementById("selectContributionType");
    songContributionSelection.innerHTML += `<option value="${songContribution}">${songContribution}</option>`;
}

function addSongToArtist(){
    const songContributionSelection = document.getElementById("selectContributionType");
    const songSelection = document.getElementById("selectSong");
    
    const createdSongContribution = {songId: songSelection.value, artistId: artistId, contributionType: songContributionSelection.value};
    fetch(`/api/SongContributions`,
        {
            method: "POST",
            body: JSON.stringify(createdSongContribution) ,
            headers: {
                "Content-Type":"application/json"
            }
        })

        .then(r => {
            if(r.status === 201){
                loadSongsOfArtist();
                loadAddSongList();
                return showChangesConfirmation()
            }
        })
        .catch(reason => alert("Call failed: " + reason));
}

function showChangesConfirmation(){
    const confirmationSpan = document.getElementById("confirmationSpan");
    confirmationSpan.innerText = "Changes saved!"
}