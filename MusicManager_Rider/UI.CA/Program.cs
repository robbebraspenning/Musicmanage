﻿using System;
using System.Collections.Generic;
using MusicManager.BL;
using MusicManager.BL.Domain;
using MusicManager.UI.CA.Extensions;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using MusicManager.DAL;
using MusicManager.DAL.EF;


namespace MusicManager.UI.CA
{
    class Program
    {
        static void Main(string[] args)
        {
            MusicManagerDbContext dbContext = new MusicManagerDbContext();
            IRepository repository = new Repository(dbContext);
            IManager manager = new BL.Manager(repository);
            Program prog = new Program(manager);
            prog.Start();
        }
        
        private readonly IManager _manager;

        public Program(IManager manager)
        {
            _manager = manager;
        }
        
        void Start()
        {
            bool running = true;

            while (running)
            {
                try
                {
                    running = PrintMenu();
                    Console.WriteLine();
                }
                catch (ValidationException e)
                {
                    Console.WriteLine("Error: " + e.ValidationResult.ErrorMessage + "Try again!");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e);
                }
            }
        }

        private bool PrintMenu()
        {
            Console.WriteLine("What would you like to do?\n" +
                              "==========================\n" +
                              "0) Quit\n" +
                              "1) Show all songs\n" +
                              "2) Show songs of genre\n" +
                              "3) Show all artists\n" +
                              "4) Show artists by Alias and/or by amount of followers\n" +
                              "5) Add an artist\n" +
                              "6) Add a song\n" +
                              "7) Add song to artist\n" +
                              "8) Remove song from artist\n" +
                              "Choice (0-8): ");

            int choice;
            bool valid = Int32.TryParse(Console.ReadLine(), out choice);

            if (valid)
            {
                switch (choice)
                {
                    case 0:
                        return false;
                    case 1:
                        Console.WriteLine();
                        PrintSongs();
                        Console.WriteLine();
                        break;
                    case 2:
                        Console.WriteLine();
                        PrintSongsByGenre();
                        Console.WriteLine();
                        break;
                    case 3:
                        Console.WriteLine();
                        PrintArtists();
                        Console.WriteLine();
                        break;
                    case 4:
                        Console.WriteLine();
                        PrintArtistsByAliasOrByAmountOfFollowers();
                        Console.WriteLine();
                        break;
                    case 5:
                        Console.WriteLine();
                        AddArtist();
                        Console.WriteLine();
                        break;
                    case 6:
                        Console.WriteLine();
                        AddSong();
                        Console.WriteLine();
                        break;
                    case 7:
                        Console.WriteLine();
                        AddSongToArtist();
                        Console.WriteLine();
                        break;
                    case 8:
                        Console.WriteLine();
                        RemoveSongFromArtist();
                        Console.WriteLine();
                        break;
                }
            }

            return true;
        }

        private void PrintSongs()
        {
            foreach (Song song in _manager.GetAllSongsWithArtists())
            {
                Console.WriteLine(song.GetInfo());
            }
        }

        private void PrintSongsByGenre()
        {
            Console.WriteLine("Select genre (Rap = 1,Hyperpop = 2, Pop = 3,Rnb = 4,Edm = 5, Rock = 6, Jazz = 7, Classical = 8, Other = 9): ");

            bool valid = false;

            do
            {
                int choice;
                valid = Int32.TryParse(Console.ReadLine(), out choice);

                if (valid && (choice > 0 && choice < 10))
                {
                    foreach (Song song in _manager.GetSongByGenre((Genre) choice))
                    {
                        Console.WriteLine(song.GetInfo());
                    }
                }
                else
                {
                    Console.WriteLine("Not a valid number! Try again!");
                    valid = false;
                }

            } while (!valid);

        }

        private void PrintArtists()
        {
            foreach (Artist artist in _manager.GetAllArtistsWithRecordLabel())
            {
                Console.WriteLine(artist.GetInfo());
            }
        }

        private void PrintArtistsByAliasOrByAmountOfFollowers()
        {
            Console.WriteLine("Enter (a part) of the artist name or leave blank: ");
            string alias = Console.ReadLine();

            bool valid = false;
            int amountOfFollowers;
            do
            {
                Console.WriteLine("Amount of followers > or leave blank : ");
                string input = Console.ReadLine();
                valid = Int32.TryParse(input, out amountOfFollowers);

                if (!valid && input != "")
                {
                    Console.WriteLine("Not a valid number! Try again!");
                } else if (!valid && input == "")
                {
                    valid = true;
                }
                
            } while (!valid);
            
            foreach (Artist artist in _manager.GetArtistsByAliasOrByAmountOfFollowers(alias,amountOfFollowers))
            {
                Console.WriteLine(artist.GetInfo());
            }
        }

        private void AddArtist()
        {
            Console.WriteLine("Enter the name of the artist: ");
            string name = Console.ReadLine();

            Console.WriteLine("Enter the alias of the artist: ");
            string alias = Console.ReadLine();


            bool valid = false;
            int amountOfFollowers;
            do
            {
                Console.WriteLine("Enter the amount of followers the artist has: ");
                valid = Int32.TryParse(Console.ReadLine(), out amountOfFollowers);
                
                if(!valid) Console.WriteLine("Error: That is not a valid number.");
                
            } while (!valid);

            _manager.AddArtist(name, alias, null, 0, amountOfFollowers, null, null);
        }

        
        private void AddSong()
        {
            Console.WriteLine("Enter the title of the song: ");
            string title = Console.ReadLine();

            bool validLength = false;
            TimeSpan length;
            do
            {
                Console.WriteLine("Enter the length of the song (format = mm:ss): ");
                validLength = TimeSpan.TryParseExact(Console.ReadLine(), "mm\\:ss", CultureInfo.CurrentCulture, out length);
                
                if(!validLength) Console.WriteLine("Error: That is not a valid length.");
                
            } while (!validLength);
            
            bool validReleaseDate = false;
            DateTime releaseDate;
            do
            {
                Console.WriteLine("Enter the release date of the song (format = dd/MM/yyyy): ");
                validReleaseDate = DateTime.TryParseExact(Console.ReadLine(), "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None,out releaseDate);

                if(!validReleaseDate) Console.WriteLine("Error: That is not a valid date.");
                
            } while (!validReleaseDate);
            
            bool validAmountOfStreams = false;
            long? amountOfStreams = null;
            do
            {
                Console.WriteLine("Enter the amount of streams the song has (Positive number): ");
                string input = Console.ReadLine();
                long tempAmountOfStreams;
                validAmountOfStreams = Int64.TryParse(input, out tempAmountOfStreams);

                if (!validAmountOfStreams && !String.IsNullOrEmpty(input))
                {
                    Console.WriteLine("Error: That is not a valid amount of streams (Positive number).");
                } else if (String.IsNullOrEmpty(input))
                {
                    validAmountOfStreams = true;
                }
                else
                {
                    amountOfStreams = tempAmountOfStreams;
                }
            } while (!validAmountOfStreams);
            
            bool validGenre = false;
            int genre;
            do
            {
                Console.WriteLine("Enter the genre of the song (Rap = 1,Hyperpop = 2, Pop = 3,Rnb = 4,Edm = 5, Rock = 6, Jazz = 7, Classical = 8, Other = 9): ");
                validGenre = Int32.TryParse(Console.ReadLine(), out genre);
                
                if(!validGenre) Console.WriteLine("Error: That is not a valid number.");
                
            } while (!validGenre);

            _manager.AddSong(title,amountOfStreams,DateTime.MinValue, length,null,(Genre)genre);
        }

        private void AddSongToArtist()
        {
            IList<Artist> artists = _manager.GetAllArtistsWithRecordLabel().ToList();
            
            if (artists.Count == 0)
            {
                Console.WriteLine("No artists found.");
                return;
            }
            
            Console.WriteLine("Which artist would you like to add a song to?");
            for(int i = 0;i<artists.Count();i++)
            {
                Console.WriteLine($"[{artists[i].Id}] {artists[i].GetInfo()}");
            }

            bool validArtistId = false;
            int artistId;
            do
            {
                Console.WriteLine("Please enter an artist ID: ");
                validArtistId = Int32.TryParse(Console.ReadLine(), out artistId);

                if (validArtistId && (artistId < 1 || artistId > artists.Count()))
                {
                    validArtistId = false;
                }

            } while (!validArtistId);

            IList<Song> songs = _manager.GetSongsNotOfArtist(artistId).ToList();
            
            if (songs.Count == 0)
            {
                Console.WriteLine("No songs found.");
                return;
            }
            
            IList<int> songIds = new List<int>();
            foreach(Song song in songs) songIds.Add(song.Id);
            
            Console.WriteLine("Which song would you like to add to the artist?");
            for(int i = 0;i<songs.Count();i++)
            {
                Console.WriteLine($"[{songs[i].Id}] {songs[i].GetInfo()}");
            }
            
            bool validSongId = false;
            int songId;
            do
            {
                Console.WriteLine("Please enter a song ID: ");
                validSongId = Int32.TryParse(Console.ReadLine(), out songId);

                if (validSongId && (!songIds.Contains(songId)))
                {
                    validSongId = false;
                }

            } while (!validSongId);

            _manager.AddSongContribution(songId, artistId);

        }

        private void RemoveSongFromArtist()
        {
            IList<Artist> artists = _manager.GetAllArtistsWithRecordLabel().ToList();

            if (artists.Count == 0)
            {
                Console.WriteLine("No artists found.");
                return;
            }
            
            Console.WriteLine("Which artist would you like to remove a song from?");
            for(int i = 0;i<artists.Count();i++)
            {
                Console.WriteLine($"[{artists[i].Id}] {artists[i].GetInfo()}");
            }

            bool validArtistId = false;
            int artistId;
            do
            {
                Console.WriteLine("Please enter an artist ID: ");
                validArtistId = Int32.TryParse(Console.ReadLine(), out artistId);

                if (validArtistId && (artistId < 1 || artistId > artists.Count()))
                {
                    validArtistId = false;
                }

            } while (!validArtistId);
            
            IList<Song> songs = _manager.GetSongsOfArtist(artistId).ToList();
            
            if (songs.Count == 0)
            {
                Console.WriteLine("No songs found for this artist.");
                return;
            }
            
            IList<int> songIds = new List<int>();
            
            foreach(Song song in songs) songIds.Add(song.Id);
            
            Console.WriteLine("Which song would you like to remove from the artist?");
            for(int i = 0;i<songs.Count();i++)
            {
                Console.WriteLine($"[{songs[i].Id}] {songs[i].GetInfo()}");
            }
            
            bool validSongId = false;
            int songId;
            do
            {
                Console.WriteLine("Please enter a song ID: ");
                validSongId = Int32.TryParse(Console.ReadLine(), out songId);

                if (validSongId && (!songIds.Contains(songId)))
                {
                    validSongId = false;
                }

            } while (!validSongId);

            _manager.RemoveSongContribution(songId, artistId);
        }
        
    }
}